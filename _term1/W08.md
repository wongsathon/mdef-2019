---
title: The Almost Useful Machines
layout: "page"
order: 08
---
description: Educational one week class on design mechanism to create Almost Useful Artifacts/MACHINES Week - (TAUM)
Autors: Eduardo Chamorro, Santiago Fuentemilla
---

###### tags: `MDEF` `FLU` `Future Learning Unit` `Fab Lab Barcelona`

[TOC]

# The Almost Useful Artifacts/MACHINES Week - (TAUM)

## Syllabus and Learning Objectives

![](https://uploads-ssl.webflow.com/5c04712c90375e6299527929/5c6ff7229b547705a4f78369_0036_MEDIA_Lineup_01.png)

Fab Labs and advanced manufacturing infrastructure are making accessible for any citizen to make anything anywhere while sharing it with global networks of knowledge, which allows accelerating design, development and deployment processes for new products to be born. Traditional planning and urbanism are being disrupted by the acceleration of technology and the dynamic transformation of society during the last half century; it is important to rethink how we make things and why, and generate active and practical conversations through projects and prototypes that become manifests itself.

**TAUM** is a practical and intensive one-week experimental program into fabrication and  introduction to the Fab Lab environment. It has been designed to fill knowledge gaps and aimed to prepare students to succeed and improve their experience during [Fab Academy](http://fabacademy.org/).

We will be going over the basic skills needed to design, develop and fabricate almost anything in a Fab Lab, as well as how to manage time and resources necessary to its proper operation.

Our active learning methodology is based on the practice and **spiral development**, designed to encourage the creativity and imagination of the participants, as well as stimulate the search for tools and solutions for their correct definition.

We will offer an impact experience, seeking to inspire and motivate the participants to use the possibilities of digital manufacturing and technologies to prototype, design, fabricate and program **an “honest” mechanical artefact that “makes” something.**


## Useless machines

As existential purity, building a machines that doesn´t have a clear purpose as fabricating something or solve worlds problems allows the designer to focus on mechanics and movements allowing more freedom on to really simplify actuation forgotting about constrains.

The metaphor of machines and artifacts doing endless predefined or random movements is what we call **Useless Machines**.

**Students will develop and fabricate something that is a mess of contradictions and wonderfulness.**

![](https://tinkerlog1.files.wordpress.com/2012/09/s_img-3491.jpg)

[VIDEO LINK](https://vimeo.com/50053703) - *ThinkerLog*

<iframe width="560" height="315" src="https://www.youtube.com/embed/2-iqu1EOcyo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*ThinkerLog



### Context

It has been a long tradition among philosophers and writers to praise uselessness as a means to stress the importance of spiritual activities and creations without clear functional aims. Aristotle, for one, established early on that knowledge was valuable in itself, not for providing practical utility—a notion frequently forgotten today. To praise inutility, thus, has been a reaction to the materialistic values promoted by capitalist society, which has been criticized for its lack of moral and spiritual values. 

![](https://www.geek.com/wp-content/uploads/2014/06/21.gif)
*A pair of machines get useless together. One device has an electromagnet on the end of its arm, which it uses to drop a pile of iron filings in front of the other one. The second machine has a broom that pushes the dust right back where it belongs. Sunrise, sunset.* 

Because machines are generally associated with the fulfillment of a practical duty, the functional independence of art is particularly highlighted when artists create or represent machines. We find ultimate examples of useless art machines in Wim Delvoye’s pursuit of technologically sophisticated devices for the production of excrement, or in Roxy Paine's machines to fabricate art, which we can consider two times "useless": for being artworks and for producing more art.

### Examples

<iframe src="https://player.vimeo.com/video/132609771" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/132609771">The random user</a> from <a href="https://vimeo.com/monobo">MONOBO</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

*The random user - Arturo Melero

![](https://cc447eceeacd37832785-b64aa34c15204933b7cd8a82d90a8d37.ssl.cf1.rackcdn.com/04_arm_useless_machines.jpg)

*Bryan Cera-Useless Machines*

![](https://gitlab.com/MDEF/mdef-2018/raw/master/assets/images/aea617f146c7-Armengol.jpg)

<iframe width="560" height="315" src="https://www.youtube.com/embed/l_PpfoZ0tBk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[*Desktop Makes*](https://www.desktopmakes.com/single-post/2017/05/23/Twist-Vase-Useless-Machine)

<iframe width="560" height="315" src="https://www.youtube.com/embed/-Lm48xH6PaY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Peter Fischli and David Weiss, The Way Things Go, 1987. Video projection.*

![](https://i2.wp.com/gothamtogo.com/wp-content/uploads/2019/03/Wim_Devoye_Travel_Kit1.jpg?resize=924%2C811&ssl=1)
*2009-2010, Mixed media, 78 x 53 x 26 cm. Wim Devoye, Cloaca Travel Kit, 2009-1*

<iframe width="560" height="315" src="https://www.youtube.com/embed/-iL96ucOpRw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Machine with Ball Chain - Arthur Ganson*

## Schedule

### Total Duration

* Classes: Mon-Thu (15 per week)
* Student work hours: Mon-Thursday (13 per week)
* Total hours per student: 28 hours

**Presentation: Thursday From 13 to 14h.**


### Calendar

#### Monday
- Morning session 10h(sharp)to 14h
    - **10:00 - 11:00** -> Introduction + Artifacts prototype DEMO + Q&A
    - **11:00 - 11:30** -> Group formation
        - Skills start
    - **11:30 - 12:30** -> `GROUP WORKING` Design brainstorming
        - Output: 2 ideas + references
    - **12:30 - 13:30** -> `CONTINUOUS REVIEWs` Design brainstorming (Tutors goinng group by group)
    - **13:30 - 14:00** -> Conclusions and group ideas presentation(sharing concepts)
- Afternoon session 15-19h
    - **15:00 - 17:00** -> `GROUP WORKING` First prototypes
    - **17:00 - 19:00** -> `CONTINUOUS REVIEWs` Fabrication an materializing methodology (by tutors)

#### Tuesday
- Morning session 10h(sharp)to 14h
    - **10:00 - 10:30** -> From Design to Replicability
    - **10:30 - 14:00** -> `GROUP WORKING` v0.0 Artifacts fabrication
- Afternoon session 15-19h
    - **15:00 - 16:00** -> `CONTINUOUS REVIEWs` Fabrication/mechatronics/electronics (by tutors)
    - **16:00 - 19:00** -> `GROUP WORKING` v1.0 Artifacts fabrication
    
#### Wednesday
- Morning session 10h(sharp)to 14h
    - **10:00 - 11:30** -> Troubleshooting (by tutors)
    - **11:30 - 14:00** -> `GROUP WORKING` Final production of artifacts V2.0
- Afternoon session 15-19h
    - **15:00 - 19:00** -> Final  artifacts V2.0 and videoshoot of working prototypes,pictures,final presentation start
    
#### Thursday - Final day
- Morning session 10h(sharp)to 14h
    - **10:00 - 12:00** -> Videoshoot of working prototypes,pictures,final presentation and video/media editing.
    - **12:00 - 14:00** -> **Artifacts presentation**
    - **14:00 - 15:00** -> Learning Dimensions

## Output

**“Honest” mechanical artifact (1 input + 2 output + replicable + 2 differents fabrication process)**


## Materials and resources

***Techniques***
    - Laser Cutter
    - 3d Printing
    - CNC machining
    - Recycling
    - Hand tools
    - Welding
    - Soldering

***Materials***
- Arduinos unos (max 2 per group)
- Basic set of sensors
    - Distance
    - Light
    - Sound
    - Touch
    - Magnetic
    - Button
    - Temp/humidity
- Basic set of actuators
    - Steppers(max 1 per group)
    - Servos
    - DC motors
    - Water/air pumps
    - Electromagnet
    - Relay 
    - LED
- Each team will have a power supply 12v 10amp
- 1x Wood 12-15(based on availability)1200x1200mm max
- 1x Wood or acrylic 3-5mmm 800x600mm max
- Cables

## Working methodology

The whole class will be divided in groups of 4 people.
Each team will present a **ARTIFACT** at the end of the course.

### Presentation Requirements

- Video at minimun 1080p stabilized (not hand held recordings, use a tripod if you don´t know how to stabilize by software)
- Black or white background.
- Open source music matching the artifacts(properly acknowledged).
- Five photographies of the artifacts at high res.
- Ideally the sound produced by the machine will be also recorded in the video.
- Entry and finish titles with Team names,name of the artifact and Iaac/FablabBCN - Flu Logos.
- Min 10 slides presentation on:
    - How you designed it
    - What is supose to do or not to do.
    - How you fabricate it
    - Accomplishments/failures
- All the documentation needs to specify what part of the project was developed by each team member.

### Goals
- Maximize uselness with the minimum amount of hardware
- Minimalism is key word
- Visually attractive machine
- All the element must be seen accounted for "INTEGRATIVE DESIGN"
-  

### Grading Method

    Documentation, 40%
    Involvement(individual): 20% include fill the form
    Presentation: 35%
    If it does not explode: 5%

## Background Research Material

* [MDEF-FABLAB CRASH COURSE](https://hackmd.io/@fablabbcn/crashcourse)
* [From Bits to Atoms 2018-19](https://bitsandatoms.gitlab.io/site/)
* [Fab Academy](http://fabacademy.org/)
* [Fab Foundation](https://www.fabfoundation.org/)
* [Fab Academy BCN](http://fab.academany.org/2018/labs/barcelona/local/general/)
* [Fab Zero](https://gitlab.fabcloud.org/fabzero/fabzero)
* [Daniel Armengol Altayo](http://fabacademy.org/archives/2015/eu/students/armengol_altayo.daniel/htm/18.html)
* [Machines that Make](http://mtm.cba.mit.edu/)
* [Designing Reality](http://designingreality.org/)

*Reference Books*

 - [Make: The Maker’s Manual: A Practical Guide to the New Industrial Revolution](https://www.amazon.es/Make-Makers-Practical-Industrial-Revolution/dp/145718592X/ref=tmm_pap_swatch_0?_encoding=UTF8&qid=1537539307&sr=1-1)
- [The 3D Printing Handbook: Technologies, design and applications](https://www.amazon.es/gp/product/9082748509/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&tag=3dhu-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=9082748509&linkId=33a88790f9237a3e0c6eae63445b1948)
 - [Fab: The Coming Revolution on Your Desktop - from Personal Computers to Personal Fabrication](https://www.amazon.es/Fab-Revolution-Personal-Computers-Fabrication/dp/0465027458/ref=pd_sim_14_4?_encoding=UTF8&pd_rd_i=0465027458&pd_rd_r=cc7aded7-f904-11e8-a234-ef904517ef02&pd_rd_w=qEj87&pd_rd_wg=GloRf&pf_rd_p=cc1fdbc2-a24a-4df6-8bce-e68491d548ae&pf_rd_r=8CBTMRHPVFRSQ0JWN5EP&psc=1&refRID=8CBTMRHPVFRSQ0JWN5EP)


## Faculty

* Santi Fuentemilla
* Xavi Dominguez
* Eduardo Chamorro
* Mikel Llobera
* Oscar Gonzalez
* Julie Mallet

## Grouping

Estrella de habilidades
- Electronics
- Programming
- Design and CAD
- Fabrication
